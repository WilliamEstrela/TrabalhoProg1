package com.company;

import javax.swing.*;
import java.awt.*;

public class CaixaDeTexto extends JPanel {


    protected JTextField imputOBJ;

    public  CaixaDeTexto(String texto, int qtd){
        setLayout(new FlowLayout());
        add(new JLabel(texto));
        imputOBJ = new JTextField(qtd);
        imputOBJ.setMaximumSize(imputOBJ.getPreferredSize());
        add(Box.createHorizontalStrut(qtd));
        add(imputOBJ);
    }

    public String getTexto() {
        String texto = imputOBJ.getText();
        return texto;
    }

    public void setTexto(String texto) {
        imputOBJ.setText(texto);
    }
}
