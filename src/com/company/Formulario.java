package com.company;

import javax.swing.*;
import java.awt.*;

public class Formulario extends JFrame{

    public Formulario (String titulo, int largura, int altura){

        super(titulo);



        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(largura,altura);

        getContentPane().add(new JanelaSimples());
        setVisible(true);
        pack();
    }


}